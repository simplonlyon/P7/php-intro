<?php
// pour pouvoir accéder simplement à la base de données, il faut inclure le fichier sql.php dans notre code
include '../src/sql.php';

// le fichier sql.php fourni un outil (DBConnection) pour envoyer des requêtes dans la base de données
// en fonction du type de requête que l'on veut, il faut appeler select, insert, update ou delete dessus avec '::'
// par exemple, pour selectionner quelque chose dans la base de données on écrit 'DBConnection::select()'
// on peut ensuite mettre dans les parenthèses notre requête SQL sous forme d'une string.
// ici on envoie la requette 'SELECT * FROM car' dans la base de données :
$result = DBConnection::select("SELECT * FROM car");
var_dump($result);
// la variable $result contient le tableau de résultat de la requête, on peut donc naviguer dedans avec les crochets '$variable['ligne']['cellule']'
echo "La voiture n° " . $result[2]['id'] . " est une " . $result[2]['brand'] . " " . $result[2]['model'];

// Si on veut passer des variables à notre requête SQL, il faut les passer dans un tableau après la requete :
// DBConnection::select("MA REQUÊTE SQL", [ "MA" => "VARIABLE" ])
// ┌────────────────────────────────────┘
// └> attention à ne pas oublier la virgule qui sépare la requête et le tableau !
//
// pour associer les valeurs à la requête, on utilise la syntaxe ':clé'
// dans la requête et on place la valeur en face de cette clé dans le tableau :
// DBConnection::select("SELECT * FROM car WHERE brand = :clé", [ "clé" => 'valeur' ]);
//                                                         └────────┘
// dans cet exemple, SQL remplace ':clé' par 'valeur' dans la requête.
// ici, on cherche à selectionner toutes les voitures de marque peugeot qui coûtent plus de 100 centimes d'euro :
$whereBrand = "peugeot";
$result = DBConnection::select("SELECT * FROM car WHERE brand = :a AND price > :b", [
    "a" => $whereBrand,
    "b" => 100
]);
// on peut aussi bien mettre des variables que directement des valeurs dans le tableau.
//
// pour des raisons de sécurité, il ne faut SOUS AUCUN PRÉTEXTE concatener les variables avec la requête.

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
    <!--
    pour récupérer un formulaire envoyé par le client, il faut que les attributs 'action' et 'method' soit complétés.
    'action' permet de dire au navigateur sur quel URL on souhaite envoyer le formulaire.
    'method' permet de dire au navigateur quelle méthode HTTP doit être utilisée pour envoyer les données au server, on utilise POST 99% du temps.

    ici, on demande au navigateur d'envoyer le formulaire en POST sur l'url bdd.php (ce fichier) :
    -->
    <form action="bdd.php" method="POST">
        <!-- 
        il est fondamental de mettre un attribut 'name' sur chaque inputs (à l'exception du submit) pour que le tableau $_POST soit utilisable
        -->
        <input type="text" name="brand" placeholder="marque" required>
        <input type="text" name="model" placeholder="modèle" required>
        <input type="number" name="price" min="0" step="0.01" placeholder="prix">
        <input type="submit">
    </form>
    <?php
    // quand quelque chose est envoyé en POST à PHP, cette chose est stockée dans le tableau $_POST
    // on peut donc récupérer les valeurs d'un formulaire HTML grâce à $_POST

    if (count($_POST) != 0) {
        DBConnection::insert("INSERT INTO car (brand, model, price) VALUES (:a, :b, :c)", [
            "a" => $_POST["brand"],
            "b" => $_POST["model"],
            "c" => $_POST["price"] * 100,
        ]);
    }

    ?>
</body>
</html>