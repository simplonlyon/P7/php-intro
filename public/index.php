<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="asset/css/bootstrap.css" />
</head>
<body>
    <?php echo "<h1>Hello World !</h1>"; ?>

    <!-- on peut mettre plusieurs balises php dans la même page -->
    <?php

    // on déclare une variable et on lui donne un nombre entier comme valeur
    $display = 4;

    // les 'if' permettent de "prendre une décision" par rapport à la valeur d'une variable.
    // si le 'if' est vrai (true) alors le code dans les accolades va s'executer.
    // 'else' quand à lui permet d'executer du code si le 'if' est faux.

    // if (la condition est vraie) {
    //     ... on execute ce code.
    // } else {
    //     ... sinon, on execute ce code-ci.
    // }

    // ici, on vérifie si la variable $display est égale à 4 grâce à l'opérateur '=='.
    // Attention à ne pas confondre '=' qui permet de donner une valeur à une variable (affectation)
    // et '==' qui permet de vérifier si une valeur est égale à une autre (comparaison).

    if ($display == 1) {
        echo '<div class="alert alert-success" role="alert">
        A simple success alert—check it out!
        </div>';
    } else if ($display == 2) { // on peut enchainer les 'if' et les 'else'.
        echo '<div class="alert alert-warning" role="alert">
        A simple success alert—check it out!
        </div>';
    } else {
        echo '<div class="alert alert-danger" role="alert">
        A simple success alert—check it out!
        </div>';
    }

    // en programmation, les boucles donnent la possibilité de répeter bes blocs de code.
    // pour écrire une boucle en php, on peut utiliser 'while' ou 'for

    // un 'while' execute le code entre ses accolades en boucle tant que sa condition entre parenthèse est vraie.

    // while (la condition est vraie) {
    //     ... on execute ce bloc en boucle
    // }

    // si on ne fait rien, la boucle peut tourner théoriquement jusqu'à l'infinni (jusqu'à ce que la RAM soit pleine et que le PC crash).
    // il faut donc veiller à ce que cela n'arrive pas en s'assurant que la condition du 'while' finnisse toujours par devenir fausse.
    // pour cela, on peut utiliser une variable que l'on incrémente à chaque passage dans le 'while',
    // il nous suffi juste ensuite de s'assurer dans la condition que l'on ne dépasse pas un nombre de tours de boucle donné.
    $count = 1;

    while ($count <= 2) { // on s'assure que la boucle ne tourne que deux fois.
        echo '<div class="alert alert-success" role="alert">
        A simple success alert—check it out!
        </div>'; // on fait ce qu'on a à faire
        $count = $count + 1; // et on incrémente le compteur
    }

    // même exemple avec un 'for' :
    for ($count=0; $count <= 2; $count++) { 
        echo '<div class="alert alert-success" role="alert">
        A simple success alert—check it out!
        </div>'; // on fait ce qu'on a à faire
    }
    // un for permet précisément de boucler un nombre de fois défini en utilisant une variable pour compter les boucles (un compteur).
    // si on compare le 'while' et le 'for', on peut voir qu'on y retrouve les même instructions.
    //
    // for ($count=0; $count <= 2; $count++)
    // la structure dans les parenthèses change un petit peu, on voit qu'il peut y avoir trois membres séparés par des points virgules.
    // en premier, on déclare notre compteur
    // en second, on donne la condition de la boucle
    // en troisième, on incrémente le compteur ('$variable++' est un raccourci pour '$variable = $variable + 1')
    // on peut voir le for comme un raccourci pour écrire un while avec un compteur.

    // un tableau permet de mettre plusieurs valeurs dans une seule variable.
    // il se déclare entre crochet '[]'.
    // chaques éléments du tableau sont séparés avec des virgules, comme quand un humain énumère des choses à l'écrit...
    $tab = [1, 1, 2, 3, 5, 8, 13];
    // quand on range une ou plusieurs valeur dans un tableau, PHP va leur donner un index pour les retrouver :
    // [
    //     0 => "primier élément rangé dans un tableau",
    //     1 => "second élément rangé dans un tableau",
    //     2 => "troisième élément rangé dans un tableau",
    // ]
    // 
    // par défaut, le premier élement est indexé à 0, le second à 1 et ainsi de suite
    //
    // c'est possible de définir soit même la clé avec n'importe quel type de valeur, ici des strings :
    // [
    //     "un" => 1,
    //     "deux" => 2,
    //     "trois" => 3,
    // ]
    // on parle aussi de système clé-valeur ou de tableaux associatifs.
    // on peut accéder à la valeur d'un tableau à partir de son index avec la syntaxe suivante :
    echo $tab[5]; // affiche '8', le cinquième membre du tableau $tab

    // echo ne permet pas d'afficher directement un tableau, pour voir ce qu'il y a dedans, on utilise 'var_dump()'
    var_dump($tab);

    // grâce à une boucle, on peut accèder successivement à toutes les valeurs d'un tableau :
    for ($index=0; $index < count($tab); $index++) {  // count() permet d'obtenir le nombre d'éléments dans un tableau.
        echo $tab[$index];
    }

    // avec un 'while' :
    $index = 0;
    while ($index < count($tab)) {
        echo $tab[$index];
        $index++;
    }

    // boucler sur un tableau est quelque chose de très fréquent en programmation, c'est pourquoi PHP nous fourni un raccourci :
    foreach ($tab as $value) {
        echo $value;
    }
    // plus besoin de compteur, PHP nous passe directement une à une chaque valeur du tableau.
    // on peut même lui demander de nous donner la clé (index) avec :
    foreach ($tab as $key => $value) {
        echo $key;
        echo $value;
    }

    // on peut mettre n'importe quel type de valeur dans un tableau, ici des strings :
    $boutons = ["hello", "world" ,"ok"];


    foreach ($boutons as $value) {
        // on peut concatener (mettre des strings bout à bout pour n'en former qu'une seule) des string avec le point '.' :
        echo '<button type="button" class="btn btn-primary">' . $value . '</button>';
    }

    echo "<br>";

    // un tableau seul n'a qu'une seule dimension, mais on peut construire un tableau à deux dimensions en imbriquant des tableaux :
    $tictactoe = [
        ["O","X", "O"],
        ["X","O", "X"],
        ["X","O", "X"],
    ];
    // pour accèder aux valeurs d'un tableau à deux dimensions, il faut deux boucles.
    // une pour récupérer chaque lignes du tableau et une autre pour récupérer chaques valeurs dans la ligne.
    echo '<table>';
    foreach ($tictactoe as $row) { // on récupère les lignes ex : ["O","X", "O"]
        echo '<tr>';
        foreach ($row as $value) { // on récupère les valeurs ex : "O"
            echo '<td>' . $value . '</td>';
        }
        echo '</tr>';
    }
    echo '</table>';
    ?>

    <script src="asset/js/jquery-3.js"></script>
    <script src="asset/js/popper.js"></script>
    <script src="asset/js/bootstrap.js"></script>
</body>
</html>